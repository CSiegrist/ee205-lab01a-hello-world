///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01 - Hello World
//
// @author Cassidy Siegrist <cjjs@hawaii.edu>
// @date   12_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int main() {
	printf("Hello world!\n");
}

